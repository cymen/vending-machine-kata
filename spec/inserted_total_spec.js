const expect = require('chai').expect;
const insertedTotal = require('../src/inserted_total');

describe('inserted total', function() {
  it('returns 0 when no coins inserted', function() {
    expect(insertedTotal({})).to.equal(0);
  });

  it('can total a single nickel', function() {
    expect(insertedTotal({ nickel: 1 })).to.equal(5);
  });

  it('can total a single dime', function() {
    expect(insertedTotal({ dime: 1 })).to.equal(10);
  });

  it('can total a single quarter', function() {
    expect(insertedTotal({ quarter: 1 })).to.equal(25);
  });

  it('can total a nickel, dime and quarter', function() {
    expect(insertedTotal({
      nickel: 1,
      dime: 1,
      quarter: 1,
    })).to.equal(5 + 10 + 25);
  });

  it('can total when more than one coin', function() {
    expect(insertedTotal({
      nickel: 2,
      dime: 2,
      quarter: 2,
    })).to.equal(5 + 5 + 10 + + 10 + 25 + 25);
  });

  it('ignores an unknown key in passed in state', function() {
    expect(insertedTotal({ bob: 1 })).to.equal(0);
  });
});
