const chai = require('chai');
const expect = chai.expect;
const spy = require('chai').spy;
const mockery = require('mockery');
const insertedTotal = require('../src/inserted_total');

var fakeInput;
const readlineMock = {
  question: function() {
    return fakeInput;
  },
};

describe('input', function() {
  var input;

  before(function() {
    mockery.enable();
    mockery.registerMock('readline-sync', readlineMock);
    mockery.registerAllowables([
      './inserted_total',
      './decrement_amount',
      '../src/input',
    ]);
    input = require('../src/input');
  });

  after(function() {
    mockery.disable();
  });

  it('does not mutate passed in state', function() { 
    fakeInput = 'press cola';
    const state = {
      nickel: 0,
      dime: 11,
      quarter: 0,
      cola: 1,
      coinReturn: {},
    };

    const newState = input(state);

    expect(newState).not.to.equal(state);
    expect(newState.coinReturn).not.to.equal(state.coinReturn);
  });

  describe('insert', function() {
    [ 'nickel', 'dime', 'quarter' ].forEach(function(coin) {
      it('accepts insert of a ' + coin, function() {
        fakeInput = 'insert ' + coin;

        expect(input({ [coin]: 0 })).to.deep.equal({ [coin]: 1 });
      });
    });

    it('rejects insert of a penny', function() {
      fakeInput = 'insert penny';

      expect(input({})).to.deep.equal({
        coinReturn: {
          penny: 1,
        },
      });
    });

    it('correctly handles uppercase INSERT', function() {
      fakeInput = 'INSERT DIME';

      expect(input({ dime: 0 })).to.deep.equal({ dime: 1 });
    });
  });

  describe('press', function() {
    it('does not dispense a cola when no cola is available', function() {
      fakeInput = 'press cola';
      const state = {
        nickel: 0,
        dime: 0,
        quarter: 4,
        cola: 0,
      };

      expect(input(state).cola).to.equal(0);
    });

    it('does not dispense a cola when not enough money is inserted', function() {
      [
        {
          nickel: 0,
          dime: 0,
          quarter: 0,
          cola: 1,
        },
        {
          nickel: 0,
          dime: 7,
          quarter: 1,
          cola: 1,
        },
      ].forEach(function(state) {
        fakeInput = 'press cola';

        expect(input(state).cola).to.equal(1);
      });
    });

    it('dispenses a cola on press cola with enough value inserted', function() {
      [
        {
          nickel: 20,
          cola: 1,
        },
        {
          dime: 10,
          cola: 1,
        },
        {
          quarter: 4,
          cola: 1,
        },
      ].forEach(function(state) {
        fakeInput = 'press cola';

        const newState = input(state);

        expect(newState.cola).to.equal(0);
        expect(insertedTotal(newState)).to.equal(0);
      });
    });

    it('makes change on press cola with too much money inserted', function() {
      [
        {
          nickel: 0,
          dime: 11,
          quarter: 0,
          cola: 1,
        },
        {
          nickel: 1,
          dime: 3,
          quarter: 3,
          cola: 1,
        },
      ].forEach(function(state) {
        fakeInput = 'press cola';

        const newState = input(state);

        expect(newState.cola).to.equal(0);
        expect(insertedTotal(newState.coinReturn)).to.equal(insertedTotal(state) - 100);
      });
    });

    it('does not dispense chips when no chips are available', function() {
      fakeInput = 'press chips';
      const state = {
        nickel: 0,
        dime: 0,
        quarter: 2,
        chips: 0,
      };

      expect(input(state).chips).to.equal(0);
    });

    it('dispenses chips on press chips with enough value inserted', function() {
      [
        {
          nickel: 10,
          chips: 1,
        },
        {
          dime: 5,
          chips: 1,
        },
        {
          quarter: 2,
          chips: 1,
        },
      ].forEach(function(state) {
        fakeInput = 'press chips';

        const newState = input(state);

        expect(newState.chips).to.equal(0);
        expect(insertedTotal(newState)).to.equal(0);
      });
    });

    it('does not dispense chips on press chips when not enough coins inserted', function() {
      fakeInput = 'press chips';
      const state = {
        nickel: 0,
        dime: 0,
        quarter: 0,
        chips: 1,
      };

      expect(input(state)).to.deep.equal({
        nickel: 0,
        dime: 0,
        quarter: 0,
        chips: 1,
        lastAction: 'press chips',
      });
    });

    it('makes change on press chips with too much money inserted', function() {
      [
        {
          nickel: 0,
          dime: 7,
          quarter: 0,
          chips: 1,
        },
        {
          nickel: 1,
          dime: 3,
          quarter: 1,
          chips: 1,
        },
      ].forEach(function(state) {
        fakeInput = 'press chips';

        const newState = input(state);

        expect(newState.chips).to.equal(0);
        expect(insertedTotal(newState.coinReturn)).to.equal(insertedTotal(state) - 50);
      });
    });

    it('does not dispense candy when no candy is available', function() {
      fakeInput = 'press candy';
      const state = {
        nickel: 1,
        dime: 1,
        quarter: 2,
        candy: 0,
      };

      expect(input(state).candy).to.equal(0);
    });

    it('does not dispense candy on press candy when not enough coins inserted', function() {
      fakeInput = 'press candy';
      const state = {
        nickel: 0,
        dime: 0,
        quarter: 0,
        candy: 1,
      };

      expect(input(state)).to.deep.equal({
        nickel: 0,
        dime: 0,
        quarter: 0,
        candy: 1,
        lastAction: 'press candy',
      });
    });

    it('dispenses candy on press candy with enough value inserted', function() {
      [
        {
          nickel: 13,
          candy: 1,
        },
        {
          nickel: 1,
          dime: 6,
          candy: 1,
        },
        {
          nickel: 1,
          dime: 1,
          quarter: 2,
          candy: 1,
        },
      ].forEach(function(state) {
        fakeInput = 'press candy';

        const newState = input(state);

        expect(newState.candy).to.equal(0);
        expect(insertedTotal(newState)).to.equal(0);
      });
    });

    it('makes change on press candy with too much money inserted', function() {
      [
        {
          nickel: 1,
          dime: 7,
          quarter: 0,
          candy: 1,
        },
        {
          nickel: 1,
          dime: 3,
          quarter: 2,
          candy: 1,
        },
      ].forEach(function(state) {
        fakeInput = 'press candy';

        const newState = input(state);

        expect(newState.candy).to.equal(0);
        expect(insertedTotal(newState.coinReturn)).to.equal(insertedTotal(state) - 65);
      });
    });
  });

  it('returns inserted coins on return coins command', function() {
    fakeInput = 'return coins';
    const state = {
      nickel: 1,
      dime: 1,
      quarter: 2,
    };

    const newState = input(state);

    expect(newState.coinReturn).to.deep.equal(state);
    expect(insertedTotal(newState)).to.equal(0);
  });

  it('sets last action to press on press command', function() {
    [
      'cola',
      'chips',
      'candy',
    ].forEach(function(product) {
      fakeInput = 'press ' + product;

      expect(input({}).lastAction).to.equal('press ' + product);
    });
  });

  it('resets last action when input is called again', function() {
    fakeInput = 'press cola';
    let state = input({ cola: 1 });
    expect(state.lastAction).to.equal('press cola');
    fakeInput = '';

    expect(input(state).lastAction).to.be.undefined;
  });
});
