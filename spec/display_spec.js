const expect = require('chai').expect;
const display = require('../src/display');

describe('display', function() {
  it('returns INSERT COIN when no coins inserted', function() {
    expect(display({}, {})).to.equal('INSERT COIN');
  });

  it('returns the amount when coins are inserted', function() {
    expect(display({ nickel: 1, dime: 1, quarter: 1 }, {})).to.equal('0.40');
  });

  it('displays thank you when a cola, bag of chips or candy is dispensed', function() {
    expect(display({ cola: 0 }, { cola: 1 })).to.equal('THANK YOU');
    expect(display({ chips: 0 }, { chips: 1 })).to.equal('THANK YOU');
    expect(display({ candy: 0 }, { candy: 1 })).to.equal('THANK YOU');
  });

  it('displays the product price if lastAction set and no product dispensed', function() {
    expect(display({ cola: 1, lastAction: 'press cola' }, { cola: 1 })).to.equal('PRICE 1.00');
    expect(display({ chips: 1, lastAction: 'press chips' }, { chips: 1 })).to.equal('PRICE 0.50');
    expect(display({ candy: 1, lastAction: 'press candy' }, { candy: 1 })).to.equal('PRICE 0.65');
  });

  it('displays SOLD OUT if lastAction set and no product available', function() {
    expect(display({ cola: 0, lastAction: 'press cola' }, { cola: 0 })).to.equal('SOLD OUT');
    expect(display({ chips: 0, lastAction: 'press chips' }, { chips: 0 })).to.equal('SOLD OUT');
    expect(display({ candy: 0, lastAction: 'press candy' }, { candy: 0 })).to.equal('SOLD OUT');
  });
});
