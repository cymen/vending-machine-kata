module.exports = function decrementAmount(state, amount) {
  let amountDebited = 0;

  if (state.quarter && state.quarter > 0) {
    while (amount - amountDebited >= 25 && state.quarter > 0 && amountDebited < amount) {
      state.quarter -= 1;
      amountDebited += 25;
    }
  }

  if (state.dime && state.dime > 0) {
    while (amount - amountDebited >= 10 && state.dime > 0 && amountDebited < amount) {
      state.dime -= 1;
      amountDebited += 10;
    }
  }

  if (state.nickel && state.nickel > 0) {
    while (amount - amountDebited >= 5 && state.nickel > 0 && amountDebited < amount) {
      state.nickel -= 1;
      amountDebited += 5;
    }
  }
}

