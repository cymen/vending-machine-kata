const readline = require('readline-sync');
const insertedTotal = require('./inserted_total');
const decrementAmount = require('./decrement_amount');

function giveChange(state) {
  if (!state.coinReturn) {
    state.coinReturn = {};
  }

  [
    'nickel',
    'dime',
    'quarter',
  ].forEach(function(coin) {
    if (state[coin]) {
      state.coinReturn[coin] = state[coin];
      state[coin] = 0;
    }
  });
}

module.exports = function input(currentState) {
  const state = Object.assign({}, currentState);
  if (currentState.coinReturn) {
    state.coinReturn = Object.assign({}, currentState.coinReturn);
  }
  delete state.lastAction;
  const input = readline.question('> ');
  const tokens = input.toLowerCase().split(' ');
  const command = tokens[0];

  if (command === 'insert') {
    const coin = tokens[1];

    if ([ 'nickel', 'dime', 'quarter' ].indexOf(coin) !== -1) {
      state[coin] += 1;
    } else {
      if (!state.coinReturn) {
        state.coinReturn = {};
      }
      if (!state.coinReturn[coin]) {
        state.coinReturn[coin] = 0;
      }
      state.coinReturn[coin] += 1;
    }
  }

  if (command === 'press') {
    const product = tokens[1];

    if (product === 'cola') {
      if (state.cola > 0 && insertedTotal(state) >= 100) {
        state.cola -= 1;
        decrementAmount(state, 100);
        giveChange(state);
      }
      state.lastAction = 'press cola';
    }

    if (product === 'chips') {
      if (state.chips > 0 && insertedTotal(state) >= 50) {
        state.chips -= 1;
        decrementAmount(state, 50);
        giveChange(state);
      }
      state.lastAction = 'press chips';
    }

    if (product === 'candy') {
      if (state.candy > 0 && insertedTotal(state) >= 65) {
        state.candy -= 1;
        decrementAmount(state, 65);
        giveChange(state);
      }
      state.lastAction = 'press candy';
    }
  }

  if (command === 'return') {
    giveChange(state);
  }

  return state; 
}
