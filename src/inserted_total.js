module.exports = function insertedTotal(state) {
  let total = 0;

  if (state.nickel) {
    total += state.nickel * 5;
  }

  if (state.dime) {
    total += state.dime * 10;
  }

  if (state.quarter) {
    total += state.quarter * 25;
  }

  return total;
}
