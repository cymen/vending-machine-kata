const insertedTotal = require('./inserted_total');

function productDispensed(state, priorState) {
  return state.cola !== priorState.cola ||
    state.chips !== priorState.chips ||
    state.candy !== priorState.candy;
}

module.exports = function display(state, priorState) {
  if (productDispensed(state, priorState)) {
    return 'THANK YOU';
  } else if (state.lastAction) {
    switch (state.lastAction) {
      case 'press cola':
        if (state.cola > 0) {
          return 'PRICE 1.00';
        } else {
          return 'SOLD OUT';
        }
      case 'press chips':
        if (state.chips > 0) {
          return 'PRICE 0.50';
        } else {
          return 'SOLD OUT';
        }
      case 'press candy':
        if (state.candy > 0) {
          return 'PRICE 0.65';
        } else {
          return 'SOLD OUT';
        }
    }
  } else if (insertedTotal(state) === 0) {
    return 'INSERT COIN';
  }

  return (insertedTotal(state) / 100).toFixed(2);
}
