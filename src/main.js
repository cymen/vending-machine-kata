const display = require('./display');
const input = require('./input');

let state = {
  nickel: 0,
  dime: 0,
  quarter: 0,
  cola: 10,
  chips: 10,
  candy: 10,
};
let priorState = state;

while(true) {
  console.log(display(state, priorState));
  priorState = state;
  state = input(state);
}
