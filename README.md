# Vending Machine Kata

The [vending machine kata](https://github.com/guyroyse/vending-machine-kata) in JavaScript.

## Installing dependencies

If nodejs is not installed, on mac run `brew install nodejs. Then:

```sh
npm install
```

## Tests

To run tests:

```sh
npm test
```

## Running

```sh
npm start
```

## Incomplete

I TDD'ed red-green through most of it. At a certain point, I wanted to hook up the main
loop so I did so with src/main.js (opting not to test it due to time). I have tested
main loop like this before here:

https://github.com/cymen/node-ttt/tree/master/src

Remaining:

* exact change only
* maybe displaying returned coins? (not explicity mentioned in Kata)
